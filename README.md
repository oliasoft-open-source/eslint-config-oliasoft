# eslint-config-oliasoft

Common ESLint rules for Oliasoft projects.

# Usage

To install and use in a project:

```
npm install -D eslint @oliasoft-open-source/eslint-config-oliasoft
```

# Development

We utilize [yarn](https://yarnpkg.com/), so to install dependencies, use `yarn` command

## Deployment

To deploy changes:

- bump the version number in `package.json` (use [SemVer](https://semver.org/))
- submit and complete a merge request describing changes
- merging automatically publishes package
- import new version in dependent repositories
