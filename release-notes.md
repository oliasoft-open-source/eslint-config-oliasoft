# ESLint Config Release Notes

## 1.3.1

- Patch gitlab ci file

## 1.3.0

- Remove babel parser and plugin ([OW-20426](https://oliasoft.atlassian.net/browse/OW-20426))
- Remove package-lock.json (using yarn)
- update README

## 1.2.4

- Upgrade to Node 22

## 1.2.3

- Remove lines between class members

## 1.2.2

- Fix mattermost url and changed channel name to Release bots [OW-14759](https://oliasoft.atlassian.net/browse/OW-14759)

## 1.2.0

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 1.2.0

- first publish of release notes
